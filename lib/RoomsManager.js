/**
 * Менеджер комнат. Предполагается один экземпляр на  приложение.
 */
class RoomsManager {
  constructor() {
    this.rooms = [];
  }

  /**
   * Добавить комнату в индекс.
   * @param {ChatRoom} room
   */
  addRoom(room) {
    this.rooms.push(room);
  }

  /**
   * Проверка на существование комнаты.
   * @param {number} id
   * @returns {bool}
   */
  isRoomExists(id) {
    return this.rooms.some(room => room.id === id);
  }

  /**
   * Генерация уникального для текущего индекса ID для новой комнаты.
   */
  generateRoomId() {
    return this.rooms.length === 0
      ? 1
      : Math.max.apply(null, this.rooms.map(room => room.id)) + 1;
  }

  /**
   * Поиск комнаты по ID.
   * @param {number} id
   */
  findRoom(id) {
    return this.rooms.find(room => room.id === id);
  }

  /**
   * Удаление комнаты
   * @param {number} id
   */
  deleteRoom(id) {
    this.rooms = this.rooms.filter(room => room.id !== id);
  }

  /**
   * Получить массив id всех комнат
   */
  get roomIds() {
    return this.rooms.map(room => room.id);
  }
}

module.exports.RoomsManager = RoomsManager;
