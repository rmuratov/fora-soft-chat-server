const { ChatRoom } = require('./ChatRoom.js');
const {
  NEW_MESSAGE,
  USER_JOINED_THE_ROOM,
  USER_LEFT_THE_ROOM,
  ROOM,
} = require('./eventNames.js');

/**
 * Участник чата. Создается один при соединении.
 */
class Talker {
  /**
   *
   * @param {Server} io Ссылка на экзеспляр серверв.
   * @param {Socket} socket Сокет текущего соединения.
   * @param {RoomsManager} roomsManager Ссылка на экзеспляр глобального менеджера комнат.
   */
  constructor(io, socket, roomsManager) {
    this.roomsManager = roomsManager;
    /** Никнейм */
    this.name = 'Anonymos';
    this.id = socket.id;
    this.io = io;
    this.socket = socket;
    /** Текущая комната */
    this.room = null;
  }

  /**
   * Присоединение к комнате. Уведомляет комнату о новом участнике.
   * Добавляет участника в экземпляр комнаты. Возвращает на клиент
   * номер комнаты.
   * @param {ChatRoom} room
   * @param {string} name Никнейм
   */
  joinRoom(room, name) {
    this.name = name;
    this.room = room;
    this.room.addMember({ id: this.id, name });
    this.socket.join(room.id, () => {
      this.socket
        .to(room.id)
        .emit(USER_JOINED_THE_ROOM, { name: this.name, id: this.id });
      this.socket.emit(ROOM, { room });
    });
  }

  /**
   * Генератор обработчика события нового сообщения.
   * Отправляет сообщение другим клиентам комнаты и
   * добавляет в иторию комнаты.
   */
  createNewMsgHandler() {
    return msg => {
      if (this.room) {
        const msgWithTime = { ...msg, dateTime: Date.now() };
        this.room.newMessage(msgWithTime);
        this.io.to(this.room.id).emit(NEW_MESSAGE, msgWithTime);
      }
    };
  }

  /**
   * Вспомогательная функция.
   * Берет ID комнаты, который пришел с клиента.
   * Возвращает комнаты с таким ID или создает новую.
   * @param {any} room ID комнаты с клиента.
   */
  getOrCreateRoom(room) {
    let roomId = parseInt(room, 10);
    if (isNaN(roomId) || !this.roomsManager.isRoomExists(roomId)) {
      roomId = this.roomsManager.generateRoomId();
      room = new ChatRoom(roomId);
      this.roomsManager.addRoom(room);
    } else {
      room = this.roomsManager.findRoom(roomId);
    }
    return room;
  }

  /**
   * Генератор обработчика события запроса на присоединение к комнате.
   * @see joinRoom
   */
  createJoinHandler() {
    return data => {
      let { room, name } = data;
      room = this.getOrCreateRoom(room);
      this.joinRoom(room, name);
    };
  }

  /**
   * Генератор обработчика дисконнекта от сервера.
   * Удаляет из комнаты, уведомляет остальных.
   */
  createDisconnectingHandler() {
    return () => {
      if (this.room) {
        this.socket
          .to(this.room.id)
          .emit(USER_LEFT_THE_ROOM, { name: this.name, id: this.id });
        this.room.removeMember(this.id);
        if (this.room.isEmpty) this.roomsManager.deleteRoom(this.room.id);
      }
    };
  }
}

module.exports.Talker = Talker;
