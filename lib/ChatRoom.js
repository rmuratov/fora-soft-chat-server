/**
 * Комната чата
 */
class ChatRoom {
  /**
   * @param {number} id - ID комнаты, целое число.
   */
  constructor(id) {
    this.id = id;
    this.members = [];
    this.messages = [];
  }

  /**
   * Добавить участника в комнату.
   * @param {Talker} talker
   */
  addMember(talker) {
    this.members.push(talker);
  }

  /**
   * Удалить участника из комнаты.
   * @param {Talker.id} id
   */
  removeMember(id) {
    this.members = this.members.filter(member => member.id !== id);
  }

  /**
   * Добавить сообщение в историю сообщений комнаты.
   * @param {object} msg
   * @param {string} msg.text
   * @param {string} name
   * @param {Date} msg.dateTime
   */
  newMessage(msg) {
    this.messages.push(msg);
  }

  /**
   * Пустая ли комната
   */
  get isEmpty() {
    return this.members.length === 0;
  }
}

module.exports.ChatRoom = ChatRoom;
