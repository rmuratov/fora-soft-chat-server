require('dotenv').config();
const server = require('http').createServer();
const io = require('socket.io')(server);
io.origins([process.env.CLIENT_HOST]);

const { RoomsManager } = require('./lib/RoomsManager.js');
const { Talker } = require('./lib/Talker.js');
const {
  NEW_MESSAGE,
  JOIN,
  DISCONNECTING,
  ERROR,
} = require('./lib/eventNames.js');

const roomsManager = new RoomsManager();

io.on('connection', function handleIoConnection(socket) {
  console.log(`${socket.id} connected`);
  const talker = new Talker(io, socket, roomsManager);

  socket.on(JOIN, talker.createJoinHandler());
  socket.on(NEW_MESSAGE, talker.createNewMsgHandler());
  socket.on(DISCONNECTING, talker.createDisconnectingHandler());
  socket.on(ERROR, function(err) {
    console.log('Received error from socket:', socket.id);
    console.log(err);
  });
});

const PORT = process.env.PORT;

server.listen(PORT, function(err) {
  if (err) throw err;
  console.log(`App is running http://localhost:${PORT}.`);
});
