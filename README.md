# Run

1. Run

```shell
cd server
npm install
```

2. Create .env file with `PORT` env var, i.e.:

```
PORT=3001
```

3. Run `node index.js`
